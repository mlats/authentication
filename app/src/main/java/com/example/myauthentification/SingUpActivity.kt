package com.example.myauthentification

import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sing_up.*

class SingUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sing_up)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        SignUpButton.setOnClickListener {
            signup()
        }
    }

    private fun signup (){
        val email:String = emailEditText.text.toString()
        val password:String = PasswordEditText.text.toString()
        val repassword:String= PasswordReEditText.text.toString()

        if(email.isNotEmpty() && password.isNotEmpty())
        {
            if (password==repassword)
            {
                ProgressBar.visibility= View.VISIBLE
                SignUpButton.isClickable=false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        ProgressBar.visibility=View.GONE
                        SignUpButton.isClickable=true
                        if (task.isSuccessful) {
                            d("SignUp", "createUserWithEmail:success")
                            val user = auth.currentUser
                        } else {
                            w("signUp", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT).show()
                        }
                    }

            } else { Toast.makeText(this,"check your password",Toast.LENGTH_SHORT).show()}

        } else {
            Toast.makeText(this, "Please fill the fields",Toast.LENGTH_SHORT).show()}
    }
}

