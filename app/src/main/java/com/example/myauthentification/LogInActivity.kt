package com.example.myauthentification

import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*

class LogInActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
    }

    private fun init(){
        auth = Firebase.auth
        LogInButton.setOnClickListener {
        Login()
        }
    }

    private fun Login()
    {
        val email:String = emailEditText.text.toString()
        val password:String = PasswordEditText.text.toString()
        
        if(email.isNotEmpty() && password.isNotEmpty()) {
            ProgressBar.visibility= View.VISIBLE
            LogInButton.isClickable=false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    ProgressBar.visibility= View.GONE
                    LogInButton.isClickable=true
                    if (task.isSuccessful) {
                        d("Login", "signInWithEmail:success")
                        val user = auth.currentUser
                    } else {
                        w("Login", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }

        } else {Toast.makeText(this, "Please fill the fields", Toast.LENGTH_SHORT).show()} }
    }

